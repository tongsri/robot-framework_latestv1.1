*** Settings ***
Resource          ../../Settings/Library.txt
Resource          ../Excel/ExecuteTestExcel.txt
Resource          ../Excel/DataValidation.txt
Resource          ../../SETTINGS/AllResource.txt

*** Keywords ***
KEYWORD STATUS END
    [Arguments]    ${ScreenShot}
    Set Suite Variable    ${KEYWORD_STATUS}    ${STEP_STATUS}
    Log to Console    CurrenRowStep --> ${currentRowStep}
    [EW] Write Cell EndStep To Excel    TestStep    ${currentRowStep}    ${KEYWORD_STATUS}    ${ScreenShot}
    Set Suite Variable    ${runningStep}    ${${runningStep} + 1}
    Set Suite Variable    ${currentRowStep}    ${${currentRowStep} + 1}

KEYWORD STATUS PENDING
    [Arguments]    ${KeywordName}
    Set Suite Variable    ${KEYWORD_STATUS}    FAIL
    Set Suite Variable    ${KEYWORD_NAME}    ${KeywordName}
    Log to Console    KEYWORD_NAME --> ${KEYWORD_NAME}
    FOR    ${i}    IN    ${KeywordName}
        log    [Pending] ${i}
        Log To Console    [Pending] ${i}
    END
    Log to Console    CurrenRowStep --> ${currentRowStep}
    [EW] Write Cell StartStep To Excel    TestStep    ${currentRowStep}    ${KEYWORD_NAME}

KEYWORD STATUS TEARDOWN
    FOR    ${i}    IN    ${KEYWORD_NAME}
        log    [${KEYWORD_STATUS}] ${i}
        Log To Console    [${KEYWORD_STATUS}] ${i}
    END

WAIT FOR PAGE CONTAINS ELEMENT
    [Arguments]    ${Element}    ${TimeOut}=10s
    ${Return}    Run Keyword And Ignore Error    Wait Until Page Contains Element    ${Element}    ${TimeOut}
    [Return]    ${Return}

WAIT FOR PANG CONTAINS
    [Arguments]    ${Message}    ${TimeOut}=10s
    ${Return}    Run Keyword And Ignore Error    Wait Until Page Contains    ${Message}    ${TimeOut}
    [Return]    ${Return}

REPORT SET
    ${FileDate}    Get Current Date    result_format=%Y-%m-%dR%H-%M
    ${RESULT_FORMAT}    Set Variable    RESULT_${FileDate}
    ${folderDate}    Get Current Date    result_format=%Y%m%d_%H%M%S
    ${DIR}    Replace String    ${CURDIR}    RF_Framework\\KEYWORD\\GlobleKeywords    RF_Framework
    ${DIR}    Replace String    ${DIR}    RF_Framework\\KEYWORD\\GlobleKeywords    RF_Framework
    Set Suite Variable    ${DIR}    ${DIR}
    Set Suite Variable    ${Foldername}    ${DIR}${/}TEST_RESULT${/}${folderDate}
    Create Directory    ${DIR}${/}TEST_RESULT${/}${folderDate}
    Log To Console    Create Directory --> ${DIR}${/}TEST_RESULT${/}${folderDate}
    [EW] Open Excel To Write    TEST_DATA/TestController.xlsx    TestController    ${RESULT_FORMAT}
    Set Suite Variable    ${RESULT}    ${RESULT_FORMAT}
    Set Suite Variable    ${Filename}    ${Foldername}${/}${RESULT}.xlsx
    Set Suite Variable    ${SheetName}    TestController
    Set Suite Variable    ${TOTAL_EXECUTED}    0
    Set Suite Variable    ${TOTAL_PASSED}    0
    Set Suite Variable    ${TOTAL_FAILED}    0
    Set Suite Variable    ${currentRowStep}    1
    Set Suite Variable    ${KEYWORD_NAME}    xxxx
    ${ExecutedDate}    Get Current Date    result_format=%d/%m/%Y
    Set Suite Variable    ${EXECUTED_DATE}    ${ExecutedDate}
    [EW] Write Cell StartSummary To Excel    SummaryReport
    [Return]    ${RESULT}

TEST CASE STATUS END
    [Arguments]    ${Msg}=PASS
    Set Suite Variable    ${TESTCASE_STATUS}    ${Msg}

TEST CASE STATUS PENDING
    [Arguments]    ${TestCase}    ${TestCaseName}
    Set Suite Variable    ${TESTCASE_STATUS}    FAIL
    Set Suite Variable    ${testCaseStep}    ${TestCase}
    Set Suite Variable    ${TESTCASE_NAME}    ${TestCaseName}
    Set Suite Variable    ${runningStep}    1
    FOR    ${i}    IN    ${TestCaseName}
        Log To Console    ========================================================================================================================================
        Log To Console    [Pending] ${i}
        Log To Console    ========================================================================================================================================
    END

TEST CASE STATUS TEARDOWN
    FOR    ${i}    IN    ${TESTCASE_NAME}
        Log To Console    ========================================================================================================================================
        Log To Console    [${KEYWORD_STATUS}] ${i}
        Log To Console    ========================================================================================================================================
    END

END REPORT SET
    [EW] Write Cell EndSummary To Excel    SummaryReport
